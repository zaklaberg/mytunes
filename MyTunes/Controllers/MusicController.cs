﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyTunes.Models;
using MyTunes.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyTunes.Controllers
{
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        //GET: api/music
        //Returns a HomePageMusic object which contains five random artists, tracks and genres
        //Can eventually be called in the frontend every time the homepage is opened
        [HttpGet]
        public ActionResult<IEnumerable<HomePageMusic>> Get()
        {
            return Ok(new HomePageMusic()
            {
                Artists = (_musicRepository.GetFiveArtists()),
                Tracks = (_musicRepository.GetFiveTracks()),
                Genres = (_musicRepository.GetFiveGenres())
            });
        }

        //GET api/music/search?track=...
        [HttpGet("{search}")]
        public ActionResult<Track> Get(string track)
        {
            if (!String.IsNullOrEmpty(track))
            {
                return Ok(_musicRepository.Search(track));
            }
            return NotFound();
        }
    }
}
