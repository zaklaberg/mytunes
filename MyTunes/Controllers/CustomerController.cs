﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using MyTunes.Repositories;
using MyTunes.Models;

namespace MyTunes.Controllers
{
    /// <summary>
    /// Endpoint for interacting with the unsuspecting customers of MyTunes
    /// </summary>
    [ApiController]
    [Route("api/v1/customers")]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerRepository _repository;
        public CustomerController(CustomerRepository custRepo)
        {
            _repository = custRepo;
        }
        
        // api/v1/customers
        [HttpGet]
        public ActionResult<IEnumerable<CustomerDefault>> GetAllCustomers([FromQuery(Name = "sortBy")] string sortBy = null)
        {
            var customers = sortBy switch
            {
                "spending" => _repository.GetAllCustomersByAmountSpent(),
                _ => _repository.GetAllCustomers()
            };
            return Ok(customers);
        }

        // api/v1/customers/:id
        [HttpGet("{id:int}")]
        public ActionResult<CustomerDefault> GetCustomerById(int id)
        {
            var customer = _repository.GetCustomerById(id);
            return Ok(customer);
        }

        // api/v1/customers
        [HttpPost]
        public ActionResult PostCustomer(CustomerDefault customer)
        {
            bool success = _repository.AddNewCustomer(customer);
            if (!success)
                return StatusCode(500);

            return CreatedAtAction(nameof(GetAllCustomers), new { id = customer.CustomerId }, success);
        }

        // api/v1/customers/:id
        [HttpPut("{id:int}")]
        public ActionResult PutCustomer(int id, CustomerDefault customer)
        {
            if (id != customer.CustomerId)
                return BadRequest();
            
            bool success = _repository.UpdateCustomer(customer);
            if (!success)
                return StatusCode(500);
            return NoContent();
        }

        // api/v1/customers/:id/popular/genre
        [HttpGet("{id:int}/popular/genre")]
        public ActionResult<CustomerPopularGenres> GetMostPopularGenresForCustomer(int id)
        {
            var genres = _repository.GetCustomersMostPopularGenre(id);
            return Ok(genres);
        }

        // api/v1/customers/count/country
        [HttpGet("count/country")]
        public ActionResult<IEnumerable<CustomersInCountry>> GetCustomerCountPerCountry()
        {
            List<CustomersInCountry> customersPerCountry = _repository.GetCustomersPerCountry();
            return Ok(customersPerCountry);
        }
    }
}
