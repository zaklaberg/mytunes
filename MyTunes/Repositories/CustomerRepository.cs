using System.Collections.Generic;
using MyTunes.Models;
using System;
using System.Data.SqlClient;

namespace MyTunes.Repositories
{
    // Comments mostly absent, intentionally; should be clear what it does.
    public class CustomerRepository
    {
        public List<CustomerDefault> GetAllCustomers()
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            return GetAllCustomers(sql);
        }

        public List<CustomerDefault> GetAllCustomersByAmountSpent()
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, " +
                    "(SELECT SUM(invoice.total) FROM invoice WHERE invoice.CustomerId = customer.CustomerId) AS amount " +
                    "FROM customer " +
                    "ORDER BY amount DESC";

            return GetAllCustomers(sql);
        }

        public CustomerDefault GetCustomerById(int customerId)
        {
            CustomerDefault customer = null;
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerID";
            try
            {
                using SqlConnection con = new SqlConnection(ConnectionHelper.GetConnectionstring());
                con.Open();
                using SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@CustomerID", customerId);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    customer = new CustomerDefault();
                    customer.CustomerId = reader.GetInt32(0);
                    customer.FirstName = reader.GetString(1);
                    customer.LastName = reader.GetString(2);
                    customer.Country = reader.IsDBNull(3) ? "" : reader.GetString(3);
                    customer.PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4);
                    customer.Phone = reader.IsDBNull(5) ? "" : reader.GetString(5);
                    customer.Email = reader.GetString(6);
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("Failed to fetch all customers from database.");
            }

            return customer;
        }
        
        public bool UpdateCustomer(CustomerDefault customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET " +
                "FirstName = @FirstName," +
                "LastName = @LastName, " +
                "Country = @Country," +
                "PostalCode = @PostalCode," +
                "Phone = @Phone," +
                "Email = @Email" +
                " WHERE CustomerId = @CustomerID";
            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);

                success = cmd.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Failed to update customer into database. Reason: {e.Message}");
                success = false;
            }

            return success;
        }

        public bool AddNewCustomer(CustomerDefault customer)
        {
            bool success = true;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);

                success = cmd.ExecuteNonQuery() > 0 ? true : false;
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Failed to insert customer into database. Reason: {e.Message}");
                success = false;
            }
            
            return success;
        }
        
        // The most popular genre is defined as the genre from which a customer has purchased the most tracks
        public CustomerPopularGenres GetCustomersMostPopularGenre(int customerId)
        {
            string sql = "SELECT TOP 1 WITH TIES " +
                "Genre.Name " +
                "FROM Customer " +
                "INNER JOIN Invoice " +
                "ON Customer.CustomerId = Invoice.CustomerId " +
                "INNER JOIN InvoiceLine " +
                "ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Track " +
                "ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre " +
                "ON Track.GenreId = Genre.GenreId " +
                "WHERE customer.CustomerId = @CustomerID " +
                "GROUP BY Genre.GenreId, Genre.Name " +
                "ORDER BY COUNT(Genre.GenreId) DESC";

            List<string> genres = new List<string>();
            try
            {
                using SqlConnection con = new SqlConnection(ConnectionHelper.GetConnectionstring());
                con.Open();
                using SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("CustomerID", customerId);

                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    genres.Add(reader.GetString(0));
                }
            }
            catch (SqlException)
            {
                Console.WriteLine("Failed to fetch most popular genre.");
            }

            return new CustomerPopularGenres { CustomerId = customerId, MostPopularGenre = genres.ToArray() };
        }

        // Gets the number of customers in each country
        public List<CustomersInCountry> GetCustomersPerCountry()
        {
            var customersPerCountry = new List<CustomersInCountry>();

            string sql = "SELECT COUNT(customer.CustomerId) as ccount, country FROM customer GROUP BY country ORDER BY ccount desc";
            try
            {
                using SqlConnection con = new SqlConnection(ConnectionHelper.GetConnectionstring());
                con.Open();
                using SqlCommand cmd = new SqlCommand(sql, con);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var customersInCountry = new CustomersInCountry 
                    {
                        Country = reader.GetString(1),
                        NumberOfCustomers = reader.GetInt32(0)
                    };

                    customersPerCountry.Add(customersInCountry);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Failed to collect customers per country. Reason: {e.Message}");
            }

            return customersPerCountry;
        }

        private List<CustomerDefault> GetAllCustomers(string sql)
        {
            var customers = new List<CustomerDefault>();
            try
            {
                using SqlConnection con = new SqlConnection(ConnectionHelper.GetConnectionstring());
                con.Open();
                using SqlCommand cmd = new SqlCommand(sql, con);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CustomerDefault cust = new CustomerDefault()
                    {
                        CustomerId = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.IsDBNull(3) ? "" : reader.GetString(3),
                        PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4),
                        Phone = reader.IsDBNull(5) ? "" : reader.GetString(5),
                        Email = reader.GetString(6)
                    };
                                
                    customers.Add(cust);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine($"Failed to fetch all customers from database. Reason: {e.Message}");
            }

            return customers;
        }
    }
}