using MyTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Repositories
{

    public class MusicRepository
    {
        //These three following methods get five random of artist/genre/track by using SELECT TOP 5 ... ORDER BY NEWID()
        //Which is the way to get random rows in sql server
        public List<Artist> GetFiveArtists()
        {
            List<Artist> artistList = new List<Artist>();
            string sql = "SELECT TOP 5 ArtistID, Name FROM Artist ORDER BY NEWID()";
            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Artist temp = new Artist
                    {
                        ArtistID = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                    artistList.Add(temp);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"exception: {ex}");
            }
            return artistList;
        }

        //This method joins Track table with album, artist, album and genre because
        //a track name is pretty useless without knowing at least the artist of the track
        public List<Track> GetFiveTracks()
        {
            List<Track> trackList = new List<Track>();
            string sql = "SELECT TOP 5 TrackID, t.Name, ar.Name, al.Title, g.Name FROM Track t " +
                $"JOIN Album al ON t.AlbumId = al.AlbumId " +
                $"JOIN Genre g ON t.GenreId = g.GenreId " +
                $"JOIN Artist ar ON al.ArtistId = ar.ArtistId " +
                $"ORDER BY NEWID()";
            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Track temp = new Track
                    {
                        TrackID = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Artist = reader.GetString(2),
                        Album = reader.GetString(3),
                        Genre = reader.GetString(4)
                    };
                    trackList.Add(temp);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Something went wrong when getting tracks: {ex}");
            }
            return trackList;
        }

        public List<Genre> GetFiveGenres()
        {
            List<Genre> genreList = new List<Genre>();
            string sql = "SELECT TOP 5 GenreID, Name FROM Genre ORDER BY NEWID()";
            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Genre temp = new Genre
                    {
                        GenreID = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                    genreList.Add(temp);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Something went wrong when getting genres: {ex}");
            }
            return genreList;
        }

        //Searches through the db for tracks for the string in the url after api/music/search?track=...
        public List<Track> Search(string searchString)
        {
            List<Track> searchResults = new List<Track>();

            string sql = $"SELECT TrackID, t.Name, ar.Name, al.Title, g.Name FROM Track t " +
                $"JOIN Album al ON t.AlbumId = al.AlbumId " +
                $"JOIN Genre g ON t.GenreId = g.GenreId " +
                $"JOIN Artist ar ON al.ArtistId = ar.ArtistId " +
                $"WHERE t.Name LIKE '%{searchString}%'";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                using SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Track temp = new Track
                    {
                        TrackID = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Artist = reader.GetString(2),
                        Album = reader.GetString(3),
                        Genre = reader.GetString(4)
                    };
                    searchResults.Add(temp);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Something went wrong when getting tracks: {ex}");
            }
            return searchResults;
        }
    }
}
