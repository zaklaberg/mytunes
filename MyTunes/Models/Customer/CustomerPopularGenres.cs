﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    public class CustomerPopularGenres
    {
        public int CustomerId { get; set;  }
        public string[] MostPopularGenre { get; set; }
    }
}
