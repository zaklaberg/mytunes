﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models
{
    public class CustomersInCountry
    {
        public string Country { get; set; }
        public int NumberOfCustomers { get; set; }
    }
}
