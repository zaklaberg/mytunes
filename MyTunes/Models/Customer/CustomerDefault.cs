using System.ComponentModel.DataAnnotations;

// Model used as primary input/output for accessing customers
namespace MyTunes.Models
{
    public class CustomerDefault
    {
        public int CustomerId { get; set; } = 0;
        
        [StringLength(40), Required]
        public string FirstName { get; set; }
        
        [StringLength(20), Required]
        public string LastName { get; set; }

        [StringLength(40)]
        public string Country { get; set; } = "";

        [StringLength(10)]
        public string PostalCode { get; set; } = "";

        [StringLength(24)]
        public string Phone { get; set; } = "";
       
        [EmailAddress, Required]
        public string Email { get; set; }
    }
}