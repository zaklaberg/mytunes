using System.Collections.Generic;

namespace MyTunes.Models
{
    //The model for the response of five random tracks, artists and genres which will appear on the homepage
    public class HomePageMusic
    {
        public List<Track> Tracks { get; set; }
        public List<Artist> Artists { get; set; }
        public List<Genre> Genres { get; set; }
    }
}
