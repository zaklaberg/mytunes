namespace MyTunes.Models
{
    public class Track
    {
        public int TrackID { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Genre { get; set; }
    }
}
