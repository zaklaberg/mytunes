namespace MyTunes.Models
{
    public class Album
    {
        public int AlbumID { get; set; }
        public string Title { get; set; }
        public string ArtistID { get; set; }
    }
}
