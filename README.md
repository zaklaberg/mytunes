# MyTunes API
Access the majestic MyTunes music store using this marvelous API. 

## Usage
To run the server, first make sure the appropriate database is available on your machine. Then replace the `DataSource` in `repositories/ConnectionHelper.cs` with your own. After that, build, run the server and... have... fun...

## Endpoints
- `[GET] api/music`: Retrieves 5 random artists, tracks and genres.
- `[GET] api/music/search?track=SEARCH_QUERY`: Search for tracks containing supplied text.
- `[GET] api/v1/customers?sortBy=SORT_VAL`: Retrieves all customers, optionally sorted by `SORT_VAL`. Currently only `SORT_VAL = spending` is supported.
- `[POST] api/v1/customers`: Add a new customer to MyTunes. Expected in JSON. Requires `FirstName`, `LastName` and `Email`. `Phone`, `Country` and `PostalCode` are optional.
- `[PUT] api/v1/customers/:id`: Update an existing customer. All fields supplied to `POST` are expected here, plus a `CustomerId`, supplied both in JSON and as a query parameter.
- `[GET] api/v1/customers/count/country`: Returns the number of customers in each country with a non-zero amount of customers.
- `[GET] api/v1/customers/:id/popular/country`: Returns the most popular genre for a customer with given `id`. In case of a tie, all tied genres are returned.

Every endpoint access costs €50. If billing information has not been supplied, we'll sue your ass.
